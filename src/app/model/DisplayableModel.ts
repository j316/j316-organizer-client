export interface DisplayableModel {

  getDescription(): string;
  getTitle(): string;

}
